# aws-apigateway-kinesis-athena

![architecture](architecture.png)


## Setting up Infrastructure

* API Gateway: create IAM Role for AWS Integration

name: api-gateway-website-events-stream-role
policy: (attach managed policy)

1. AmazonAPIGatewayPushToCloudWatchLogs
2. AmazonKinesisFullAccess

* API Gateway: create api gateway proxy to kinesis
* Kinesis Data Stream: create a kinesis data stream (name: website_events_stream)
* Kinesis Firehose: create a firehose with name website_events_firehose. It creates role automatically
* S3 Bucket: ambid-website-events
* Create Glue Data Catelog Table with the following message schema

## Create Glue Metadata Table for each event type

* message type 1


| column name | source database type |
| ------ | ------ |
| id | varchar(255) |
| pageId | varchar(500) |
| siteId | varchar(45) |
| domain | varchar(45) |
| ipAddress | varchar(45) |
| uri | varchar(205) |
| referrer | varchar(45) |
| enteredPage | datetime |
| browserLanguage | varchar(45) |
| userLanguage | varchar(45) |
| systemLanguage | varchar(45) |
| browser | varchar(45) |
| browserCodeName | varchar(45) |
| javaEnabled | varchar(45) |
| platform | varchar(45) |
| cpuClass | varchar(45) |
| operatingSystem | varchar(45) |
| javascriptVersion | varchar(45) |
| screen | varchar(45) |
| colors | varchar(45) |
| cookies | varchar(45) |
| historyLength | varchar(45) |
| countryCode | varchar(45) |
| countryName | varchar(45) |
| city | varchar(45) |
| region | varchar(45) |
| latitude | varchar(45) |
| longitude | varchar(45) |
| active | char(1) |
| subscriber | char(1) |
| payload | varchar(1500) |






## Test

#### 1. sending events through API Gateway

![Test Api Gateway](test_apigateway.png)

input the following information into API Test Console


* {stream-name}

website_events_stream

* Query Strings

empty

* Headers

empty


* Request Body

```json
{
    "records": [
        {
            "data": "{
                \"id\":\"test_id_1\", 
                \"pageId\":\"test_pageId_1\", 
                \"siteId\":\"test_siteId_1\",
                \"domain\":\"test_domain_1\",
                \"ipAddress\":\"test_ipAddress_1\",
                \"uri\":\"test_uri_1\",
                \"referrer\":\"test_referrer_1\",
                \"enteredPage\":\"test_enteredPage_1\",
                \"browserLanguage\":\"test_browserLanguage_1\",
                \"userLanguage\":\"test_userLanguage_1\",
                \"systemLanguage\":\"test_systemLanguage_1\",
                \"browser\":\"test_browser_1\",
                \"browserCodeName\":\"test_browserCodeName_1\",
                \"javaEnabled\":\"test_javaEnabled_1\",
                \"platform\":\"test_platform_1\",
                \"cpuClass\":\"test_cpuClass_1\",
                \"operatingSystem\":\"test_operatingSystem_1\",
                \"javascriptVersion\":\"test_javascriptVersion_1\",
                \"screen\":\"test_screen_1\",
                \"colors\":\"test_colors_1\",
                \"cookies\":\"test_cookies_1\",
                \"historyLength\":\"test_historyLength_1\",
                \"countryCode\":\"test_countryCode_1\",
                \"countryName\":\"test_countryName_1\",
                \"city\":\"test_city_1\",
                \"region\":\"test_region_1\",
                \"latitude\":\"test_latitude_1\",
                \"longitude\":\"test_longitude_1\",
                \"active\":\"test_active_1\",
                \"subscriber\":\"test_subscriber_1\",
                \"payload\":\"test_payload_1\"
            }",
            "partition-key": "siteA"
        },
        {
            "data": "{
                \"id\":\"test_id_2\", 
                \"pageId\":\"test_pageId_2\", 
                \"siteId\":\"test_siteId_2\",
                \"domain\":\"test_domain_2\",
                \"ipAddress\":\"test_ipAddress_2\",
                \"uri\":\"test_uri_2\",
                \"referrer\":\"test_referrer_2\",
                \"enteredPage\":\"test_enteredPage_2\",
                \"browserLanguage\":\"test_browserLanguage_2\",
                \"userLanguage\":\"test_userLanguage_2\",
                \"systemLanguage\":\"test_systemLanguage_2\",
                \"browser\":\"test_browser_2\",
                \"browserCodeName\":\"test_browserCodeName_2\",
                \"javaEnabled\":\"test_javaEnabled_2\",
                \"platform\":\"test_platform_2\",
                \"cpuClass\":\"test_cpuClass_2\",
                \"operatingSystem\":\"test_operatingSystem_2\",
                \"javascriptVersion\":\"test_javascriptVersion_2\",
                \"screen\":\"test_screen_2\",
                \"colors\":\"test_colors_2\",
                \"cookies\":\"test_cookies_2\",
                \"historyLength\":\"test_historyLength_2\",
                \"countryCode\":\"test_countryCode_2\",
                \"countryName\":\"test_countryName_2\",
                \"city\":\"test_city_2\",
                \"region\":\"test_region_2\",
                \"latitude\":\"test_latitude_2\",
                \"longitude\":\"test_longitude_2\",
                \"active\":\"test_active_2\",
                \"subscriber\":\"test_subscriber_2\",
                \"payload\":\"test_payload_2\"
            }",
            "partition-key": "siteB"
        }
    ]
}
```


#### 2. batch ad-hoc query from athena against S3 Bucket 

![athena.png](athena.png)


#### 2. real-time ad-hoc query from athena against Kinesis

possible

#### 2. real-time event trigger consumer from kinesis stream

create lambda name: website_events_kinesis_consumer
create lambda execution role: website_events_kinesis_consumer_role

* https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/website_events_kinesis_consumer?tab=configuration

minimal code

```python
import json
import base64

def lambda_handler(event, context):
    records = event["Records"]
    for record in records:
        data = base64.b64decode(record["kinesis"]["data"])
        print(data)
```

## Reference

* https://cloudonaut.io/serverless-websocket-api-api-gateway-kinesis-lambda/
* https://docs.aws.amazon.com/apigateway/latest/developerguide/integrating-api-with-aws-services-kinesis.html
* https://docs.aws.amazon.com/lambda/latest/dg/with-kinesis-example.html

## Production Hardening

* failover of the system
* monitoring of the system
* lean privilege (cloud Principle of Least Privilege)
* encryption
* load testing
* infrastructure as code
* refinement sharding and data stream partition key based on use case
* s3 bucket prefix based on use case
